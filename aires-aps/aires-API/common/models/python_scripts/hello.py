## hello.py
import sys, json

# Read data from stdin
def read_in():
    lines = sys.stdin.readlines()
    return json.loads(lines[0])

def main(argv):
    #get our data as an array from read_in()
    #hello = read_in()

    #create a numpy array
    #np_lines = np.array(lines)

    print(argv) 
    sys.stdout.flush()

#start process
if __name__ == '__main__':
    main(sys.argv[2:])
