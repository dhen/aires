'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');
const express = require('express')

//let runPy = new Promise(function(success, nosuccess) {
//
//    const { spawn } = require('child_process');
//    const pyprog = spawn('python', ['./../pypy.py']);
//
//    pyprog.stdout.on('data', function(data) {
//
//        success(data);
//    });
//
//    pyprog.stderr.on('data', (data) => {
//
//        nosuccess(data);
//    });
//});

//app.get('/', (req, res) => {
//
//    res.write('welcome\n');
//
//    runPy.then(function(fromRunpy) {
//        console.log(fromRunpy.toString());
//        res.end(fromRunpy);
//    });
//})

var app = module.exports = loopback();

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
