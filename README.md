Start the hyperledger fabric instance:
./startFabric.sh

(You might have to run the create PeerAdminCard.sh script to create the big-boss admin)

Then, cd into aires-aps
run the following to install the ap-network onto our fabric-ledger:

composer network install --archiveFile aires-aps@0.0.2.bna --card PeerAdmin@hlfv1 (if this failes, run that peer admin script)

Then, start the ap-network:

composer network start --networkName aires-aps --networkVersion 0.0.2 --networkAdmin admin --networkAdminEntrollSecret adminpw --card PeerAdmin@hlfv1 --file cred-verify-admin.card

To run the composer-playground (Check the transactions of the ledger on Port 8080), use:

composer-playground

To run the API-server (Port 3000)

composer-rest-server
