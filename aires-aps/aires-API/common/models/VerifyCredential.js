'use strict';

const Composer = require('../lib/composer.js');
const { spawn } = require('child_process');
const path = require('path');

const BusinessNetworkConnection = require("composer-client").BusinessNetworkConnection;
this.businessNetworkConnection = new BusinessNetworkConnection();
this.CONNECTION_PROFILE_NAME = "hlfv1";
this.businessNetworkIdentifier = "aires-aps";
this.businessNetworkDefinition;

businessNetworkConnection.connect(
    CONNECTION_PROFILE_NAME,
    businessNetworkIdentifier,
    "admin",
    "adminpwd"
  )
  .then(result => {
    businessNetworkDefinition = result;
    console.log("BusinessNetworkConnection: ", result);
  })
  .then(() => {
    // Subscribe to events.
    businessNetworkConnection.on('event', (event) => {
      console.log("********** business event received **********", event);
    });
  })
  // and catch any exceptions that are triggered
  .catch(function(error) {
    throw error;
  });



const getUUID = function() {
    const pyFile = path.join(__dirname, 'python_scripts/crypto_interface.py');
    const args = ['UUID', str];
    args.unshift(pyFile);
    args.unshift('-u');
    console.log("Spawning getUUID Script...");
    console.log("args: " + args);
    return spawn('python3', args);
};

const runVerify = (str) => {
    const pyFile = path.join(__dirname, 'python_scripts/hello.py');
    // const args = [verify, str];
    const args = [str];
    args.unshift(pyFile);
    args.unshift('-u');
    console.log("Spawning Verifying Script...");
    console.log("args: " + args);
    return spawn('python3', args);
};

const runAPSign = (sessID) => {
    const pyFile = path.join(__dirname, 'python_scripts/crypto_interface.py');
    const args = ['sign', sessID];
    args.unshift(pyFile);
    args.unshift('-u');
    console.log("Spawning Signing Script...");
    console.log("args: " + args);
    return spawn('python3', args);
};


module.exports = function(VerifyCredential) {
  Composer.restrictModelMethods(VerifyCredential);

  // Get AP Message for User to sign
  VerifyCredential.getUUID = function(cb) {
    var response = "UUID HERE";
    // getUUID();
    cb(null, response);
    console.log("GET Request submitted and returned the following: " + response);
  }

  // The remote method for gettingUUID
  VerifyCredential.remoteMethod(
    'getUUID', {
      http: {
        path: '/getUUID',
        verb: 'get'
      },
      returns: {
        arg: 'UUID',
        type: 'string'
      },
      description: "Get a random UUID from AP to authenticate a connection"
    }
  );

  let watchForEvent = function() {
    // Connect to the business network.
    return businessNetworkConnection.connect('admin@aires-aps', 'org.aires.network', 'admin', 'adminpw').then(() => {
    
    });
  }


  // The POST request for signing a blinded SessID
  VerifyCredential.verifyTransaction = function(pub, msg, sessID, cb) {
    // Check that the returned message is Valid
    const verifyScript = runVerify(pub);
    verifyScript.stdout.on('data', (data) => {
      console.log("public " + pub);                                                    
      console.log("UUID message" + msg); 
      
      let response = "org.aires.network.Credential#" + pub;
      VerifyCredential.create({
        "$class": "org.aires.network.VerifyCredential",
        "cred": response 
      })
      businessNetworkConnection.on('event', (event) => {
//       if(event.cred == ("resource:" + response)) {
         console.log(event);
         response = {
           "$class": "org.aires.network.VerifyNotification",
           "cred": "resource:org.aires.network.Credential#5750",
           "eventId": "1b07721afc0e5664f84e71d41c10ab9381093c55b90c755e44b9faf681b3b3fa#0",
           "timestamp": "2019-03-16T13:16:04.975Z"
         }; 
//       }
      });
      console.log("POST Request submitted and signed the SESSION_ID: " + response);
      cb(null, response);
    });
    verifyScript.stderr.on('data', (data) => {
      console.log('ERROR WTH');
    });
    verifyScript.stderr.on('close', () => {
      console.log("Closed python script");
    });
  };
  
  VerifyCredential.remoteMethod (
    'verifyTransaction',
    {
      http: {path: '/verify', verb: 'post'},
      accepts: [
	{arg: 'publicKey', type: 'string', required: true},
	{arg: 'signedMsg', type: 'string', required: true},
	{arg: 'blindedSession', type: 'string', required: true},
      ],
      returns: {arg: 'signedBlindedSess', type: 'string'},
      description: "Wrapper for verifying a credential on Ledger"
    }
  );
};
