'use strict';

const Composer = require('../lib/composer.js');

module.exports = function(VerifyCredential) {
  Composer.restrictModelMethods(VerifyCredential);
};
