/**
 * Verify credential transaction
 * @param {org.aires.network.VerifyCredential} verify 
 * @transaction
 */

async function verifyCred(verify) {
  if (verify.cred.valid) {
    // If credential is currently active (not revoked)/able to use
    return getAssetRegistry("org.aires.network.Credential")
      .then(function (assetRegistry) {
        // Update the credential in the Asset Registry
    	verify.cred.valid = false
        return assetRegistry.update(verify.cred); 
      })
      .then(() => {
        let event = getFactory().newEvent(
          "org.aires.network",
          "VerifyNotification"
        ); // Get a reference to the event specified in the modeling language
        event.cred = verify.cred;
        emit(event); // Fire off the event
      });
  }
}


