'use strict';

const Composer = require('../lib/composer.js');

module.exports = function(Authenticator) {
  Composer.restrictModelMethods(Authenticator);
};
