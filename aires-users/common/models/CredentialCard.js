'use strict';

const Composer = require('../lib/composer.js');

module.exports = function(CredentialCard) {
  Composer.restrictModelMethods(CredentialCard);
};
